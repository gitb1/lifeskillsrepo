# Learning Process


 ## What is the Feynman Technique?
   
  The **Feynman Technique** emphasizes learning through explanation.


 ## In this video, what was the most interesting story or idea for you?
  The difference between car and hiker's way of learning stood out to me. Sometimes, we can learn quickly, akin to how a car travels, but we might miss a lot compared to someone taking their time, like a hiker. 
   

 ## What are active and diffused modes of thinking?

  - **Active Mode of Thinking**: This involves intense concentration, like when studying for exams through reading, summarizing, memorizing, or creating mind maps.

   - **Diffuse Mode of Thinking**: This is a more relaxed state where our brain continues to work subconsciously, forming new connections, exploring creative pathways, and making surprising associations.


  ## According to the video, what are the steps to take when approaching a new topic? Only mention the points.
  

  1. **Deconstruct the Skill**
  2. **Learn Enough to Self-Correct**
  3. **Remove Barriers to Mistakes**
  4. **Practice At Least 20 Hours** 

  
  ## What are some of the actions you can take going forward to improve your learning process?

  - Schedule breaks in between.
  - Conduct self-introspection regularly.
  - Utilize mind mapping techniques, such as creating diagrams to organize information.
  - Ensure adequate sleep.
  - Teach oneself or others.

  



