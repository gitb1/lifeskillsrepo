# Tiny Habits


### In this video, what was the most interesting story or idea for you?

In the video, the concept of starting small with tiny habits stood out to me as the most compelling idea. Rather than attempting significant changes all at once, focusing on tiny habits is more effective. These habits comprise three components: the routine (what you do), the reward (what you get), and the cue (what triggers it). Making even minor changes to these components can significantly facilitate the formation of healthy behaviors.

### How can you use B = MAP to make making new habits easier? What are M, A and P?

According to BJ Fogg's "Tiny Habits Method" video, habit formation can be simplified using the formula B = MAP, where B represents behaviour, M stands for motivation, A denotes ability and P signifies promptness. When embarking on a new habit, such as exercising regularly, considering B = MAP can be beneficial. Clear motivation (M) helps clarify why you want to exercise, keeping ability (A) simple aids in executing the habit, and setting prompts (P) facilitates consistency, like associating exercise with an existing habit.

### Why it is important to "Shine" or Celebrate after each successful completion of habit?

Celebrating successful habit completion is vital as it reinforces the behavior in your mind. Rewarding oneself after completing a habit strengthens the habit loop. According to BJ Fogg, acknowledging small victories becomes a habit itself. By adding positive experiences, such as celebrating after executing the behavior, habits become more ingrained as your brain associates them with positive outcomes.

### In this video, what was the most interesting story or idea for you?

The most captivating idea presented in the video is the concept of improving by 1%. This notion is exemplified by the story of a cycling team that achieved significant improvement through numerous minor changes. It underscores the importance of incremental progress and how small, consistent steps can lead to substantial success over time.

### What is the book's perspective about Identity?
According to "Atomic Habits," lasting positive habits stem from embodying the identity of someone who consistently performs those actions. Integrating routines into your identity surpasses merely completing objectives. Gradual changes accumulated over time contribute to forming a positive identity around habits, emphasizing the importance of consistency in habit formation.

### Write about the book's perspective on how to make a habit easier to do

"Atomic Habits" suggests simplifying habits by categorizing them into four stages: cue, craving, response, and reward. Emphasis is placed on creating an environment conducive to habit formation, satisfying cravings immediately, and simplifying habit initiation and maintenance. The goal is to make habits enjoyable and straightforward, increasing the likelihood of consistent and effective habit formation.

### Write about the book's perspective on how to make a habit harder to do

While "Atomic Habits" does not directly address making bad habits harder, it advocates for creating environments that facilitate good habits and discourage undesirable ones. For instance, to deter unhealthy snacking, keep tempting foods out of reach. By creating an environment that supports positive behaviors and hinders negative ones, habit formation becomes more manageable.


#### Habit to Increase: Regular Coding Practice

- **Cue (When to Start):** Set a daily coding time and ensure the coding environment is readily accessible.
- **Attractiveness (Make it Fun):** Focus on coding projects aligned with personal interests and explore new technologies or languages.
- **Ease (Simplify the Start):** Begin with short coding sessions and divide tasks into manageable sections or projects.
- **Satisfaction (Feel Good About It):** Acknowledge and celebrate modest coding accomplishments, such as solving problems.


#### Habit to Decrease: Preferring Nutritious Snacks Over Unhealthy Options

- **Invisibility (Hide the Cue):** Remove unhealthy snacks from visible areas in the house.
- **Unattractiveness (Make it Less Appealing):** Educate yourself on the nutritional benefits of healthier options to reduce the appeal of unhealthy snacks.
- **Difficulty (Make it Challenging):** Plan and prepare nutritious snacks in advance to make unhealthy options less convenient.
