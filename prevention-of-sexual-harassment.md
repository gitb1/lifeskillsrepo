# Prevention of Sexual Harassment


## What kinds of behaviour cause sexual harassment?

Sexual harassment encompasses a wide range of behaviours that are  inappropriate or offensive in a sexual context. 

- **Unwanted Physical Advances:** Includes touching, groping, hugging, or kissing without consent. Persistently standing too close, invading personal space, or blocking someone's path in a sexually intimidating manner.
  
- **Sexual Comments or Jokes:** Making lewd remarks, sexual innuendos, or telling inappropriate jokes of a sexual nature can create a hostile environment.
  
- **Displaying Inappropriate Material:** Sharing or displaying sexually explicit material such as pornography in the workplace or educational setting.
  
- **Unwanted Comments on Appearance:** Making comments about someone's body, clothing, or appearance in a sexualized or objectifying manner.
  
- **Unwelcome Sexual Gestures:** Making suggestive gestures or movements of a sexual nature that make others uncomfortable. Sending sexually explicit messages, images, or videos via email, social media, or other digital platforms without consent.


## What to Do if You Face or Witness Sexual Harassment

- **Assess the Situation:** Evaluate the situation. If someone is in immediate danger, it's crucial to intervene.

- **Support the Victim:** Offer support and empathy to the victim. Let them know they are not alone and that their feelings are valid. Encourage them to speak up and offer to accompany them if they choose to report the incident.

- **Document the Incident:** Take note of the date, time, location, and details of the incident or incidents you witnessed. Document any relevant information, such as what was said or done and who was involved.

- **Report the Incident:** If the victim is unable or unwilling to report the harassment themselves, consider reporting it on their behalf, with their consent.

- **Speak Up:** If you feel safe and comfortable doing so, confront the perpetrator directly or speak up against the behaviour at the moment. Communicate that their actions are inappropriate and unacceptable.

- **Follow Up:** Follow up with the victim to ensure they are receiving the support they need and to monitor any developments in the situation.
