# Listening and Active Communication


## Steps/Strategies for Active Listening

- Focusing completely on the speaker. Maintaining eye contact, eliminating distractions, and showing genuine interest in what they're saying.
- Using positive body language to show engagement.
- If something is unclear, ask for clarification.
- Reflecting feelings can help the speaker feel heard and understood.
- Summarizing the key points after the speaker has conveyed a significant amount of information.
- Putting oneself in the speaker's shoes and trying to understand their perspective.

## Reflective Listening According to Fisher's Model

Fisher's model emphasizes Reflective Listening as a key component of effective communication. Reflective Listening involves listening carefully to what the speaker is saying and then reflecting our thoughts, feelings, and intentions in our own words.

## Obstacles in Listening Process

Several obstacles can hinder effective listening:

- When we're preoccupied with our thoughts, concerns, or agenda.
- Strong emotions such as anger, frustration, or anxiety.
- Failure to empathize with the speaker's perspective or emotions.
- Distractions in today's digital age, like smartphones or laptops.

## Improving Listening Skills

- Giving full attention.
- Maintaining eye contact.
- Avoid interrupting.
- Practicing active listening.
- Controlling our emotions.
- Empathize.

## Switching to Passive Communication Style

When interacting with authority figures or individuals in positions of power, an individual may choose to adopt a more passive communication style to avoid challenging or contradicting them directly.

## Switching to Aggressive Communication Styles

When individuals feel threatened or believe their boundaries are being violated, they may respond aggressively to assert themselves and protect their rights or possessions.

## Switching to Passive-Aggressive Communication Styles

Instead of openly expressing disagreement or dissenting opinions, individuals may use passive-aggressive tactics such as silent treatment or subtle sabotage to undermine others' efforts or decisions.

## Making Communication Assertive

- Clearly stating our intentions, requests, or opinions without beating around the bush.
- Clearly defining our boundaries and asserting them assertively but respectfully.
- Maintaining good posture, making eye contact, and using confident gestures to convey assertiveness.
- Keeping our emotions in check and remaining calm, even in challenging situations.
- Applying assertive communication skills in different contexts, such as at work, in personal relationships, social settings, to build confidence and consistency.
