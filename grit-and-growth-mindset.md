# Grit and Growth Mindset

## Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

The video emphasizes the importance of both passion and perseverance, known as "grit," for success, especially in education. It introduces the concept of a "growth mindset" and advocates for finding better ways to help children develop grit.

## Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

The video discusses how we learn and introduces the idea of a "growth mindset." It contrasts a fixed mindset, where people believe abilities are fixed, with a growth mindset, where they believe they can improve. Developing a growth mindset is vital for learning in various areas like school, sports, and work.

## What is the Internal Locus of Control? What is the key point in the video?

The video talks about the "internal locus of control," which is believing you control your life. It mentions a study where kids praised for hard work felt motivated and liked challenges, linking to having an internal locus of control. The main idea is when you believe you control outcomes, you're more motivated and successful. To build this, take on challenges and understand your actions' impact.

## What are the key points mentioned by the speaker to build a growth mindset (explanation not needed)?

The speaker gives tips for developing a growth mindset:

- Believe in Your Learning Ability
- Question Assumptions
- Make a Learning Plan
- Embrace Challenges
- Stay Strong

## What are your ideas to take action and build a Growth Mindset?

Here are practical steps to foster a growth mindset:

- Own Your Learning
- Persist in Challenges
- Dive Deep
- Find Growth in Challenges
- Learn from Setbacks
